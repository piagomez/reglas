Normas y Pautas
======================

**Contenido:**

 - [Normas Generales de la comunidad](https://gitlab.com/programadores_chile/reglas/tree/master/normas_generales.md)
 - [Normas del Grupo Facebook de la comunidad](https://gitlab.com/programadores_chile/reglas/tree/master/normas_generales.md)
 - [Pautas oficiales de la comunidad]()

**Todo miembro de la comunidad puede enviar sus sugerencias.**

**Derechos de autor**

- Este readme ha sido basado del código de conducta de la comunidad [Programadores e Informáticos Chile](https://github.com/programadoreschile/pautasynormas)

- Los iconos, ilustraciones y cualquier otra imágen utilizada, ha sido creada en base a los siguientes recursos:
[Flaticon](https://www.flaticon.com)
[Canva](https://www.canva.com)